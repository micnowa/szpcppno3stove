#include "piec.hpp"

Piec::Piec (float temperatura):temperatura_(temperatura){}

PiecDoswiadczalny::PiecDoswiadczalny (float temperatura):Piec(temperatura) {}

void PiecDoswiadczalny::setTemp (float temperatura)	// ustawia temperature pieca
{
	temperatura_=temperatura;
	powiadom ();
}

void Piec::powiadom ()
{
	for (std::vector <KontrolkaTemperatury*>::iterator it = obserwatorzy_.begin(); it != obserwatorzy_.end(); ++it)
		(*it) -> odswiez ();
}

float PiecDoswiadczalny::getTemp () const
{
	return temperatura_;
}



void Piec::dodajObserwatora (KontrolkaTemperatury* obs)
{
	obserwatorzy_.push_back(obs);
}
