#include "gfx.hpp"
#include "komendy.hpp"
#include <fstream>
#include <iostream>

int main(int argc, char *arg[])
{
	if(argc != 2)
	{
		return 0;
	}
	std::ifstream plik_z_programem(arg[1]);

	FabrykaKomend fk;
	fk.rejestruj(KomendaSkoczDo::ID, &KomendaSkoczDo::kreator);
	fk.rejestruj(KomendaRampa::ID, &KomendaRampa::kreator);
	fk.rejestruj(KomendaIzotermiczne::ID, &KomendaIzotermiczne::kreator);

	Komendy komendy;
	std::string line;
	unsigned nr_linii = 0;
	unsigned liczba_bledow = 0;
	while(std::getline(plik_z_programem, line))
	{
		nr_linii++;
		if(line[0] == '#') continue;
		Komenda *k = NULL;
		k = fk.utworz(line);
		if(k) komendy.push_back(k);
		else
		{
			std::cout << "Blad skladni w linii " << nr_linii << std::endl;
			++liczba_bledow;
		}
	}
	plik_z_programem.close();
	if(liczba_bledow)
	{
		std::cerr << "Ojoj cuś się sypi" << std::endl;
		return 1;
	}else std::cerr << "O fajnie, miziamy piecyk" << std::endl;

	ncurses_init();
	rysuj_ramke(0, 0, SCREEN_W, SCREEN_H);
	init_pair(1, COLOR_BLUE, COLOR_RED);
	init_pair(2, COLOR_YELLOW, COLOR_WHITE);
	attron(COLOR_PAIR(1) | A_BOLD);
	wypisz_tekst_srodek(SCREEN_W / 2, 0, " PICYS ");
	attroff(COLOR_PAIR(1) | A_BOLD);

	PiecDoswiadczalny piecyk(1000.0);
	Komenda::ustawPiec(&piecyk);
	KolorowaKontrolkaTemperatury termometr(5, 5, 10, 5, &piecyk, 2);
	piecyk.dodajObserwatora(&termometr);

	mvprintw(SCREEN_H - 4, 5, "Kumenda:");

	float czas = 0;
	for(Komendy::const_iterator it = komendy.begin(); it != komendy.end(); ++it)
		czas += (*it)->czasWykonania();

	mvprintw(SCREEN_H - 2, 5, "A zeszlo sie: %.3f sekundow", czas);
	refresh();

	for(unsigned i = 0; i < komendy.size(); ++i)
	{
		mvprintw(SCREEN_H - 4, 15, "%u / %u", i + 1, komendy.size());
		refresh();
		(*komendy[i])();
	}
	wait(1);
	ncurses_deinit();

//	for(Komendy::iterator it = komendy.begin(); it != komendy.end(); ++it)
//		delete *it;

	return 0;
}
