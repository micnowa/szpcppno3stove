#include "komendy.hpp"
#include <cmath>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <ctime>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <iostream>
#include <thread>

Piec* Komenda::piec_;

using namespace boost::posix_time;

void Komenda::ustawPiec (Piec *p)
{
	piec_ = p;
}

KomendaSkoczDo::KomendaSkoczDo (float temp) : skoczdo_ (temp) {}

float KomendaSkoczDo::czasWykonania ()
{
	return 0.0;
}

void KomendaSkoczDo::operator() ()
{
	piec_->setTemp (skoczdo_);
}

const boost::regex KomendaSkoczDo::ID ("skocz\\s(\\d+(\\.\\d+)?)");

Komenda *KomendaSkoczDo::kreator (const std::string &kom)
{
	boost::match_results<std::string::const_iterator> co;
	std::string::const_iterator start = kom.begin (), koniec = kom.end ();
	if (regex_search(start, koniec, co, ID))
	{
		try
		{
			float temp = boost::lexical_cast <float> (std::string (co [1].first, co[1].second));
			return new KomendaSkoczDo (temp);
		}
		catch(boost::bad_lexical_cast &)
		{
			return NULL;
		}
	}
	else
		return NULL;
}

KomendaIzotermiczne::KomendaIzotermiczne (float czas) : utrzymaj_ (czas) {}

float KomendaIzotermiczne::czasWykonania ()
{
	return utrzymaj_;
}

void KomendaIzotermiczne::operator() ()
{
	wait(utrzymaj_);
}

const boost::regex KomendaIzotermiczne::ID ("izotermiczne\\s(\\d+(\\.\\d+)?)");

Komenda *KomendaIzotermiczne::kreator (const std::string &kom)
{
	boost::match_results<std::string::const_iterator> co;
	std::string::const_iterator start = kom.begin (), koniec = kom.end ();
	if (regex_search(start, koniec, co, ID))
	{
		try
		{
			float czas = boost::lexical_cast <float> (std::string (co [1].first, co[1].second));
			return new KomendaIzotermiczne (czas);
		}
		catch(boost::bad_lexical_cast &)
		{
			return NULL;
		}
	}
	else
		return NULL;
}

KomendaRampa::KomendaRampa (float od, float szybkosc, float max) : od_ (od), szybkosc_(szybkosc), do_(max)
{
	if(od_>do_)
		szybkosc_*=-1;
	czas_=abs((do_-od_)/szybkosc_);
}

float KomendaRampa::czasWykonania ()
{
	return czas_;
}

void KomendaRampa::operator() ()
{
	piec_->setTemp (od_); //szkocz do początkowej
	const float tick=0.01;		//ustaw czs 1 ticku na 1 sekundę?
	
	while(czas_>0)
	{
		wait(tick);
		piec_->setTemp (piec_->getTemp ()+szybkosc_*tick);
		czas_-=tick;

	}
	czas_=0;
	if(piec_->getTemp ()>do_)
	{
		piec_->setTemp(do_);		//zabezpieczenie przed przestrzeleniem przy precyzji zmiennoprzecinkowej
	}
}

const boost::regex KomendaRampa::ID ("^rampa\\s(\\d+\\.?\\d+?)\\s(\\d+\\.?\\d+?)\\s(\\d+\\.?\\d+?)");

Komenda *KomendaRampa::kreator (const std::string &kom)
{
	boost::match_results<std::string::const_iterator> co;
	std::string::const_iterator start = kom.begin (), koniec = kom.end ();
	if (regex_search(start, koniec, co, ID))
	{
		try
		{
			float min = boost::lexical_cast <float> (std::string (co [1].first, co[1].second));
			float max = boost::lexical_cast <float> (std::string (co [2].first, co[2].second));
			float szybkosc = boost::lexical_cast <float> (std::string (co [3].first, co[3].second));
			return new KomendaRampa (min, szybkosc, max);
		}
		catch(boost::bad_lexical_cast &)
		{
			return NULL;
		}
	}
	else
		return NULL;
}


void FabrykaKomend::rejestruj (const boost::regex &id, kreator_komend kreat)
{
    komendy[id]=kreat;
}

Komenda *FabrykaKomend::utworz (const std::string &kom)
{
	for (std::map <boost::regex, kreator_komend>::iterator it=komendy.begin(); it != komendy.end(); ++it) //porównuje z każdym wyrażeniem regularnym
	{
		if(boost::regex_match (kom, it->first)) // Szuka, czy istnieje takie wyrazenie regularne, ktore pasuje do kom
		{
			return 	komendy[it->first](kom); // Jesli tak, wola kreator danej klasy
		}
	}
    return NULL; // Jesli nie, zwraca NULL
}

void wait(float seconds)
{
	ptime czas_start = microsec_clock::universal_time();
	time_duration czas_trwania = milliseconds ((long)(seconds*1000.0));
	ptime czas_biezacy;
	do
	{
		czas_biezacy=microsec_clock::universal_time();
		std::this_thread::sleep_for(std::chrono::nanoseconds(1));
	}
	while(czas_biezacy-czas_start<czas_trwania);

}
